node version = `16.15.0`

```bash
npm install
```

```json
{
    "dev": "vite --port 3000",
    "build:tw:wc": "npx tailwindcss -i ./src/assets/styles/index2.css -o ./src/assets/styles/tailwind2.css --watch",
    "build:tw": "npx tailwindcss -i ./src/assets/styles/index2.css -o ./src/assets/styles/tailwind2.css",
    "build": "vite build",
    "preview": "vite preview --port 5050"
}
```
