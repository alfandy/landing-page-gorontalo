import { createWebHistory, createRouter } from "vue-router";
import { authFire } from "@/firebase";
import store from "@/store";

// layouts

import Belakan from "@/layouts/Belakan.vue";
import Auth from "@/layouts/Auth.vue";
import Depan from "@/layouts/Depan.vue";
// import PageError from "@/layouts/Error.vue";

// views for Admin layout

import Settings from "@/views/admin/Settings.vue";
import Tables from "@/views/admin/Tables.vue";
import Maps from "@/views/admin/Maps.vue";
// import Dashboard from "@/views/admin/Dashboard.vue";
import Dashboard from "@/views/belakan/Dashboard.vue";
import Grup from "@/views/belakan/Grup.vue";
import Aplikasi from "@/views/belakan/Aplikasi.vue";
import Setting from "@/views/belakan/Settings.vue";

// views for Auth layout

import Login from "@/views/auth/LoginBelakan.vue";
// import Register from "@/views/auth/Register.vue";

// views without layouts

import Landing from "@/views/Landing.vue";
import Profile from "@/views/Profile.vue";
// import Index from "@/views/Index.vue";

// routes

const routes = [{
        path: "/admin",
        redirect: "/admin/dashboard",
        component: Belakan,
        meta: {
            harusLogin: true,
        },
        children: [{
                path: "/admin/dashboard",
                component: Dashboard,
            },
            {
                path: "/admin/grup",
                component: Grup,
            },
            {
                path: "/admin/aplikasi",
                component: Aplikasi,
            },
            {
                path: "/admin/setting",
                component: Setting,
            },
            {
                path: "/admin/settings",
                component: Settings,
            },
            {
                path: "/admin/tables",
                component: Tables,
            },
            {
                path: "/admin/maps",
                component: Maps,
            },
        ],
    },
    {
        path: "/auth",
        redirect: "/auth/login",
        component: Auth,
        children: [{
                path: "/auth/login",
                component: Login,
            },
            // {
            //     path: "/auth/register",
            //     component: Register,
            // },
        ],
    },
    {
        path: "/",
        component: Depan,
        children: [{
            path: "/",
            component: Landing,
        }, ],
    },
    {
        path: "/profile",
        component: Profile,
    },
    { path: "/:pathMatch(.*)*", redirect: "/" },
    // { path: "/:pathMatch(.*)*", component: PageError },
];

const router = createRouter({
    history: createWebHistory(
        import.meta.env.BASE_URL),
    routes,
});

router.beforeEach((to, from, next) => {
    if (
        (to.path === "/auth/login" || to.path === "/auth/register") &&
        store.getters.user
        // authFire.currentUser
    ) {
        next("/admin");
        return;
    }
    if (
        to.matched.some((recode) => recode.meta.harusLogin) &&
        !store.getters.user
        // !authFire.currentUser
    ) {
        next("/auth");
        return;
    }
    next();
});

export default router;