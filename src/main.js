import { createApp } from "vue";
import store from "@/store";

import Toast from "vue3-toast-single";
import "vue3-toast-single/dist/toast.css";

import Loading from "vue-loading-overlay";
import "vue-loading-overlay/dist/vue-loading.css";

// // styles
import "@fortawesome/fontawesome-free/css/all.min.css";
import "@/assets/styles/custom.css";
import "@/assets/styles/tailwind.css";
import "@/assets/styles/tailwind2.css";

import App from "@/App.vue";

// layouts
import router from "@/router";

import { useRegisterSW } from "virtual:pwa-register/vue";

const intervalMS = 60 * 60 * 1000;

const updateServiceWorker = useRegisterSW({
    onRegistered(r) {
        r &&
            setInterval(() => {
                r.update();
            }, intervalMS);
    },
});

const app = createApp(App);
app.use(router);
app.use(store);

app.use(Toast, {
    verticalPosition: "top",
    duration: 3000,
    // horizontalPosition: "right",
    closeable: true,
});
app.use(Loading);
// className: "wk-warn",
// className: "wk-info",
// className: "wk-alert",
app.mount("#app");