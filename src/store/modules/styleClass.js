const style = {
    namespaced: true,
    state: {
        color: "bg-emerald-300",
        logo: "fas fa-heart",
    },
    mutations: {
        SET_LOGO(state, color) {
            state.logo = color;
        },
        SET_COLOR(state, color) {
            state.color = color;
        },
    },
    actions: {
        setColor({ commit }, data) {
            commit("SET_COLOR", data);
        },
        setLogo({ commit }, data) {
            commit("SET_LOGO", data);
        },
    },
};

export default style;