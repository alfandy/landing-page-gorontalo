import TabelSeting from "@/store/fire/setting";
import Chache from "@/store/cache1.js";
const setting = {
    namespaced: true,
    state: {
        data: {
            judul: "",
            sub_judul: "",
            email: "",
            alamat: "",
            link_background: "",
            sosial_media: [],
        },
    },
    getters: {
        data: (state) => {
            const dataChache = Chache.get("dataSetting");
            if (dataChache) {
                return JSON.parse(JSON.stringify(dataChache));
            }
            return state.data;
        },
    },
    mutations: {
        SET_SETTING_DATA(state, data) {
            state.data = JSON.parse(JSON.stringify(data));
            Chache.set("dataSetting", state.data);
        },
    },
    actions: {
        async setSettingData({ commit }) {
            const dataChache = Chache.get("dataSetting");
            if (!dataChache) {
                const data = await TabelSeting.getData("data");
                commit("SET_SETTING_DATA", data);
                return false;
            }
        },
    },
};

export default setting;