import { reactive, ref } from "vue";

export const isLoading = ref(false);
export const state = reactive({
    status: false,
});