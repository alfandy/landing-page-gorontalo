import unor from "@/data/unor.json";
const TableUnor = {
    getAll: async() => {
        let data = [];
        try {
            const dataUnor = await unor;
            if (!dataUnor || dataUnor.length < 0) {
                throw "Tidak Ada Data";
            }
            data = dataUnor.map((el) => {
                return {
                    id: el.id,
                    name: el.name,
                    image_name: "./image/svg/" + el.logo,
                    keterangan: el.keterangan,
                    status: el.status == "non-active" ? false : true,
                };
            });
            return data;
        } catch (error) {
            throw error;
        }
    },
};

export default TableUnor;