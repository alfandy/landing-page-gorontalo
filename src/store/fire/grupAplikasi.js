import { dbFireStore } from "@/firebase.js";
import {
    collection,
    query,
    getDoc,
    doc,
    getDocs,
    where,
    documentId,
} from "firebase/firestore";
const nameTable = "grup_aplikasis";
const nametableRelasi = "aplikasis";
const Colletion = collection(dbFireStore, nameTable);
const q = query(Colletion);

const TableGrupAplikasi = {
    async useLoadIdApplikasi(id) {
        let datas = null;
        try {
            const docSnap = await getDoc(doc(dbFireStore, nameTable, id));
            if (docSnap.exists()) {
                datas = docSnap.data();
            } else {
                datas = null;
            }
            return datas;
        } catch (error) {
            const errorCode = error.code;
            const errorMessage = error.message;
            // console.table({ load: "grup", errorCode, errorMessage });
            throw errorCode;
        }
    },

    async useLoadAplikasi(id) {
        let dataAplikasis = [];
        try {
            const datas = await this.useLoadIdApplikasi(id);
            if (datas) {
                const getDataAplikasi = await getDocs(
                    query(
                        collection(dbFireStore, nametableRelasi),
                        where(documentId(), "in", datas.aplikasis),
                        where("status", "==", "active")
                        // where("show", "==", "active")
                    )
                );
                // console.table("getDataAplikasi", getDataAplikasi);
                getDataAplikasi.forEach((doc) => {
                    let data = {
                        id: doc.id,
                        ...doc.data(),
                    };
                    dataAplikasis.push(data);
                });
                return dataAplikasis;
            }
            throw { code: "Belum Ada Data" };
        } catch (error) {
            const errorCode = error.code;
            // const errorMessage = error.message;
            throw errorCode;
        }
    },
};
export default TableGrupAplikasi;