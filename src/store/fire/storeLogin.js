import { createStore } from "vuex";
import { authFire } from "@/firebase";
import {
    signInWithEmailAndPassword,
    // onAuthStateChanged,
    // signOut,
} from "firebase/auth";
import router from "@/router";
export const storeLogin = createStore({
    state: {
        user: null,
    },
    mutations: {
        SET_USER(state, user) {
            state.user = user;
        },
        CLEAR_USER(state) {
            state.user = null;
        },
    },
    actions: {
        login({ commit }, data) {
            let email = data.email;
            let password = data.password;
            return signInWithEmailAndPassword(authFire, email, password)
                .then((userCredential) => {
                    commit("SET_USER", userCredential.user);
                    router.push("/admin");
                })
                .catch((error) => {
                    const errorCode = error.code;
                    // const errorMessage = error.message;
                    // return { errorData: "asdfs", errorCode, errorMessage };
                });
        },
    },
});